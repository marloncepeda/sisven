<?php
/**
 * Access from index.php:
 */
class sisven_Controller extends ZP_Controller {
	
	public function __construct() {
		$this->app("sisven");
		$this->Files = $this->core("Files");
		$this->Templates = $this->core("Templates");
		$this->Templates->theme();
		$this->helper("alerts");
		$this->helper("sessions");
		$this->sisven_Model = $this->model("sisven_Model");
	}
	//**funciones del menu principal del sistema**

	//index es la funcion principal nos abre la primera
	//pantalla de nuestro website y nos muestra el catalogo
	//de productos
	public function index() {
		if( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="user") ){
			$this->home();
		}else{
			$this->login(); 
		}
	}
	//Home es la funcion que nos muestra el menu de administracion
	//una vez logueado en nustro website
	public function home() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->title("ultimos Archivos");
			$vars["archivos"]  = $this->sisven_Model->archivos_ultimos_user(SESSION("id"));
			$vars["view"] = $this->view("home",TRUE);
			$this->render("content",$vars); 
		}else{
			$this->login(); 
		}
	}
	//Salir es la funcion que elimina las sessiones usadas en el site
	//y nos redirig a la pantalla principal del site 
	public function salir() {	
		$logs = $this->sisven_Model->logs("Salir");		
 		unsetSessions($URL);
		$this->login(); 
	}

	public function ver_contactos() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->title("Ver Contactos");
			$vars["contactos"]  = $this->sisven_Model->ver_contactos(SESSION("id"));
			$vars["listas"]  = $this->sisven_Model->buscar_todos_buddy_list(SESSION("id"));
			$vars["usuarios"]  = $this->sisven_Model->buscar_todos_usuarios();
			$vars["view"] = $this->view("contactos/home",TRUE);
			$this->render("content",$vars);

			$logs = $this->sisven_Model->logs("Ver Contactos");

		}else{
			$this->login(); 
		}
	}

	public function crear_contactos(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("crear")){
				$data = $this->sisven_Model->crear_contacto();
				//____($data);
				$logs = $this->sisven_Model->logs("crear contacto");
				echo "<script>alert('Se ha creado Correctamente');</script>";
				$this->ver_contactos();
			}else{
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("contactos/home",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}
	}
	public function borrar_contacto(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("borrar_contacto")){
				$data = $this->sisven_Model->borrar_contacto();
				//____($data);
				$logs = $this->sisven_Model->logs("borrar contacto");
				echo "<script>alert('Se ha borrrado Correctamente');</script>";
				$this->ver_contactos();
			}else{
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("contactos/home",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}	
	}
	public function perfil_usuario(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("modificar")){
				$data = $this->sisven_Model->modificar_perfil(SESSION("id"));
				echo "<script>alert('Datos Guardados con Exito Vuelva a Entrar al Sistema');</script>";
				$logs = $this->sisven_Model->logs("Modificar Perfil");
				$this->salir();
			}else{
				$data = $this->sisven_Model->buscar_logs(SESSION("ci"));
				//____($data);
				$vars["logs"] = $data;
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("perfil",TRUE);
				$this->render("content",$vars);
				$logs = $this->sisven_Model->logs("Ver Perfil");

			}
		}else{
			$this->login(); 
		}
	}


	public function crear_lista(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("guardar")){
				$data = $this->sisven_Model->crear_lista();
				//____($data);
				echo "<script>alert('Se ha creado Correctamente');</script>";
				$logs = $this->sisven_Model->logs("Crear Lista");
				$this->ver_contactos();
			}else{
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("contactos/home",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}
	}
	public function registrar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registro")){
				if(POST("nacionalidad")==""){
					echo "<script>alert('no puedes dejar la nacionalidad en blanco');</script>";
				}else{
					$data = $this->sisven_Model->registrar();
					echo "<script>alert('Se ha guardado Correctamente');</script>";
					$this->login();
				}
			}else{
				$this->home();	
			}
		}else{
			if(POST("registro")){
				if(POST("nacionalidad")==""){
					echo "<script>alert('no puedes dejar la nacionalidad en blanco');</script>";
				}else{
					$data = $this->sisven_Model->registrar();
					echo "<script>alert('Se ha guardado Correctamente');</script>";
					$this->login();
				}
			}else{
				$this->title("Registro");
				$vars["view"] = $this->view("registrar",TRUE);
				$this->render("content",$vars);
			}
		}
	}
	//Login es la funcion encargada de manipular el acceso al sistema
	public function login() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->home();
		}else{
			if(POST("entrar")){
				$data = $this->sisven_Model->login();
				//____($data);
				if($data == false){
					echo "<script>alert('no tiene cuenta creada'); document.location.href='';</script>";
				}else{
					SESSION("tipo_user", $data[0]["tipo_user"]);
					SESSION("apellido", $data[0]["apellido"]);
					SESSION("nombre", $data[0]["nombre"]);
					SESSION("id", $data[0]["id"]);
					SESSION("fecha_user", $data[0]["fecha_nac"]);
					SESSION("correo", $data[0]["correo"]);
					SESSION("ci", $data[0]["ci"]);
					
					$logs = $this->sisven_Model->logs("Entrar");
					
					
					$this->home();
				}
			}elseif(POST("registrar")){
				$this->registrar();
			}else{
				$this->title("Inicio");
				$this->CSS("login", "sisven");
				$vars["view"] = $this->view("login",TRUE);
				$this->render("content",$vars);
			}
		}
	}
	
	public function listas_usuarios() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->buscar_todos_usuarios();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay usuarios en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Usuarios");
				$vars["usuarios"] = $data;
				$vars["view"] = $this->view("contactos/todos_lista_global",TRUE);
				$this->render("content",$vars);
				$logs = $this->sisven_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}

	public function usuarios_options(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("borrar")){
				$data = $this->sisven_Model->usuario_borrar(POST("archivo_id"));
				echo "<script>alert('usuario Borrado con exito');</script>";
				$logs = $this->sisven_Model->logs("Borrar Usuario");	
				$this->listas_usuarios();
			}elseif(POST("bloquear")){
				$data = $this->sisven_Model->usuario_estado(POST("archivo_id"),"bloqueado");
				$logs = $this->sisven_Model->logs("Bloquear Usuario");
				$this->listas_usuarios();
			}elseif(POST("desbloquear")){
				$data = $this->sisven_Model->usuario_estado(POST("archivo_id"),"activo");
				$logs = $this->sisven_Model->logs("Desbloquear Usuario");
				$this->listas_usuarios();
			}
		}else{
			$this->login(); 
		}
	}

	public function subir(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("subir")){
			
				$pptx ="application/vnd.openxmlformats-officedocument.presentationml.slideshow";
				$docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
				$doc = "application/msword";
				$ppt ="application/vnd.ms-powerpoint";
				$pdf = "application/pdf";
				$archivo = FILES("userfile", "type");
				//$namefile = $_FILES['userfile']['name'].".pdf";

				if( ($archivo == $pptx) || ($archivo == $docx) || ($archivo == $doc) || ($archivo == $ppt) ){
					
					$uploaddir = '/var/www/sisven/www/lib/files/';
					$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
					$namefile = basename($_FILES['userfile']['name']).".pdf";

					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
						chmod($uploadfile, 0755);
				   		exec("python /var/www/sisven/converter.py ".$uploadfile." ".$uploadfile.".pdf");
				    	echo "<script>alert('El archivo es válido y fue cargado exitosamente.')</script>";
				    	
				    	$data = $this->sisven_Model->archivo_guardar($namefile);
						$logs = $this->sisven_Model->logs("Subir Archivo");
				    	
					} else {
					    echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					    $this->subir();
					}	
				}elseif($archivo == $pdf){
					
					$uploaddir = '/var/www/sisven/www/lib/files/';
					$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
				    	echo "<script>alert('El archivo es válido y fue cargado exitosamente.')</script>";
						$data = $this->sisven_Model->archivo_guardar($namefile);
						$logs = $this->sisven_Model->logs("Subir Archivo");
						
					} else {
					    echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					}
				}else{
					echo "<script>alert('El documento no posee una extencion valida'); document.location.href='';</script>";
				}
				unlink($uploadfile);
				$this->home();
		
			}else{
				$this->title("Subir Archivo");
				$vars["view"] = $this->view("archivo/subir",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}
	}
	public function subir_archivo_adjunto(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("subir")){
			
				$pptx ="application/vnd.openxmlformats-officedocument.presentationml.slideshow";
				$docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
				$doc = "application/msword";
				$ppt ="application/vnd.ms-powerpoint";
				$pdf = "application/pdf";
				$archivo = FILES("userfile", "type");
				//$namefile = $_FILES['userfile']['name'].".pdf";

				if( ($archivo == $pptx) || ($archivo == $docx) || ($archivo == $doc) || ($archivo == $ppt) ){
					
					$uploaddir = '/var/www/sisven/www/lib/files/';
					$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
					$namefile = basename($_FILES['userfile']['name']).".pdf";

					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
						chmod($uploadfile, 0755);
				   		exec("python /var/www/sisven/converter.py ".$uploadfile." ".$uploadfile.".pdf");
				    	echo "<script>alert('El archivo es válido y fue cargado exitosamente.')</script>";
				    	
				    	$data = $this->sisven_Model->archivo_adjuntar($namefile,POST("cant_files"));
						$logs = $this->sisven_Model->logs("Subir Archivo Adjunto");
				    	
					} else {
					    echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					    $this->subir();
					}	
				}elseif($archivo == $pdf){
					
					$uploaddir = '/var/www/sisven/www/lib/files/';
					$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
				    	echo "<script>alert('El archivo es válido y fue cargado exitosamente.')</script>";
						$data = $this->sisven_Model->archivo_adjuntar($namefile,POST("cant_files"));
						$logs = $this->sisven_Model->logs("Subir Archivo Adjunto");
						
					} else {
					    echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					}
				}else{
					echo "<script>alert('El documento no posee una extencion valida'); document.location.href='';</script>";
				}
				unlink($uploadfile);
				$this->home();
			}
		}else{
			$this->login(); 
		}
	}
	public function archivos_buscar() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivos_buscar(POST("buscar"));
			//____($data);
			if($data == false){
				echo "<script>alert('No se encontraron Documentos');</script>";
				$this->home();
			}else{
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/buscar_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->sisven_Model->logs("Buscar Archivos");
					
			}
		}else{
			$this->login(); 
		}
	}

	public function archivos_todos() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivos_todos_user(SESSION("id"));
			//____($data);
			if($data == false){
				echo "<script>alert('no tiene documentos guardados');</script>";
				$this->home();
			}else{
				$this->title("Todos los Archivo");
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/ver_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->sisven_Model->logs("Ver Archivos Todos");
					
			}
		}else{
			$this->login(); 
		}
	}
	public function archivos_publicos() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivos_publicos_user(SESSION("id"));
			//____($data);
			if($data == false){
				echo "<script>alert('no tiene documentos guardados');</script>";
				$this->home();
			}else{
				$this->title("Archivos Publicos");
				$vars["title"] = "Publicos";
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/ver_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->sisven_Model->logs("Ver Archivos Publicos");
					
			}
		}else{
			$this->login(); 
		}
	}
	public function archivos_privados() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivos_privados_user(SESSION("id"));
			//____($data);
			if($data == false){
				echo "<script>alert('no tiene documentos guardados');</script>";
				$this->home();
			}else{
				$this->title("Archivos Privados");
				$vars["title"] = "Privados";
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/ver_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->sisven_Model->logs("Ver Archivos Privados");
					
			}
		}else{
			$this->login(); 
		}
	}
	public function archivos_favoritos() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivos_favoritos_user(SESSION("id"));
			//____($data);
			if($data == false){
				echo "<script>alert('no tiene documentos guardados');</script>";
				$this->home();
			}else{
				$this->title("Archivos Favoritos");
				$vars["title"] = "Favoritos";
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/ver_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->sisven_Model->logs("Ver Archivos Favoritos");
					
			}
		}else{
			$this->login(); 
		}
	}
	public function archivos_papelera() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivos_papelera_user(SESSION("id"));
			//____($data);
			if($data == false){
				echo "<script>alert('no tiene documentos guardados');</script>";
				$this->home();
			}else{
				$this->title("Papelera");
				$vars["title"] = "Papelera";
				$vars["archivos"] = $data;
				$vars["view"] = $this->view("archivo/ver_archivo",TRUE);
				$this->render("content",$vars);
				
				$logs = $this->sisven_Model->logs("Ver Archivos Papelera");
					
			}
		}else{
			$this->login(); 
		}
	}

	public function archivos_guardar_status() {
		if(POST("papelera")){
			$data = $this->sisven_Model->archivos_guardar_status(POST("archivo_id"),"papelera");
			//____($data);
			$logs = $this->sisven_Model->logs("Cambiar estado Archivo");
			$this->archivos_papelera();
		}elseif(POST("publico")){
			$data = $this->sisven_Model->archivos_guardar_status(POST("archivo_id"),"publico");
		//	____($data);
			$logs = $this->sisven_Model->logs("Cambiar estado Archivo");
			$this->archivos_publicos();
		}elseif(POST("privado")){
			$data = $this->sisven_Model->archivos_guardar_status(POST("archivo_id"),"privado");
		//	____($data);
			$logs = $this->sisven_Model->logs("Cambiar estado Archivo");
			$this->archivos_privados();
		}elseif(POST("favorito")){
			$data = $this->sisven_Model->archivos_guardar_status(POST("archivo_id"),"favorito");
		//	____($data);
			$logs = $this->sisven_Model->logs("Cambiar estado Archivo");
			$this->archivos_favoritos();
		}elseif(POST("perfil")){
			$logs = $this->sisven_Model->logs("Ver Info Archivo");
			$this->archivos_perfil(POST("archivo_id"));
		}elseif(POST("borrar")){
			$data = $this->sisven_Model->archivo_borrar(POST("archivo_id"));
			//$this->archivos_borrar(POST("archivo_id"));
			$logs = $this->sisven_Model->logs("Borrar Archivo");
			$this->archivos_papelera();
		}
	}
	public function archivos_perfil($archivo_id){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$data = $this->sisven_Model->archivo_buscar($archivo_id);
			$data1 = $this->sisven_Model->archivo_adjuntos_buscar($archivo_id);	
			$vars["info_archivo"] = $data;
			$vars["archivos_adjuntos"] = $data1;
			$vars["view"] = $this->view("archivo/perfil_archivo",TRUE);
			$this->render("content",$vars);
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function archivos_modificar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("modificar")){
				if( POST("archivo_titulo_viejo") == POST("titulo")){
					$this->home();
				}else{
					$data = $this->sisven_Model->archivos_cambiar_nombre(POST("archivo_id"),POST("titulo"));
					exec("mv /var/www/sisven/www/lib/files/".POST('archivo_titulo_viejo')." /var/www/sisven/www/lib/files/".POST("titulo").".pdf");
					echo "<script>alert('Cambio de nombre realizado con Exito');</script>";
					$logs = $this->sisven_Model->logs("Modificar Archivo");
					$this->home();
				}

			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function respaldar_bd(){
		if(SESSION("tipo_user")=="admin"){
			echo "<script> document.location.href='http://localhost/sisven/www/applications/sisven/controllers/backupdb.php'; alert('se ha guardado con exito')</script>";
			$logs = $this->sisven_Model->logs("Respaldar BD");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function respaldar_files(){
		if(SESSION("tipo_user")=="admin"){
			exec("tar -zcvf sisven_files.tar /var/www/sisven/www/lib/files/ ");
			echo "<script>alert('El respaldo comprimido de los ficheros se encuenta en la carpeta raiz del sistema');</script>";
			$logs = $this->sisven_Model->logs("Respaldar Ficheros");
			echo $this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function reporte_usuarios_global(){
		if(SESSION("tipo_user")=="admin"){	
			echo "<script> document.location.href='http://localhost/sisven/www/classes/reporte_usuarios.php';</script>";
			$logs = $this->sisven_Model->logs("Reporte Usuarios");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_archivos_global(){
		if(SESSION("tipo_user")=="admin"){	
			echo "<script> document.location.href='http://localhost/sisven/www/classes/reporte_documentos.php';</script>";
			$logs = $this->sisven_Model->logs("Reporte Archivos");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function reporte_logs_global(){
		if(SESSION("tipo_user")=="admin"){
			echo "<script> document.location.href='http://localhost/sisven/www/classes/reporte_logs.php';</script>";
			$logs = $this->sisven_Model->logs("Reporte Logs");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
}
