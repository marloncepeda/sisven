<div class="container" >
  <div class="row">
      <div class="col-md-5 col-md-offset-1">
          <div class="login-panel panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Entrar</h3>
              </div>
              <div class="panel-body">
                  <form action="<?php print $href; ?>" method="post">
                    <fieldset>
                      <div class="form-group">
                          <input class="form-control" placeholder="Usuario" name="usuario" type="text" autofocus required>
                      </div>
                      <div class="form-group">
                          <input class="form-control" placeholder="Password" name="clave" type="password" required>
                      </div>
                      <input class="btn btn-lg btn-success btn-block" name="entrar" type="submit" value="Entrar" />
                      <a class="btn btn-lg btn-success btn-block" href="<?php print path("sisven/registrar/"); ?>">Registrarse<a/>                      
                    </fieldset>
                  </form>
              </div>
          </div>

      </div>
      <div class="col-lg-4 col-md-offset-1 mensag-panel">
                    <div class="panel panel panel-success">
                        <div class="panel-heading">
                            <p align="center">SISVEN AVISO</p>
                        </div>
                        <div class="panel-success">
                            <p class="labeel" align="justify">
                              SISVEN es la solucion software de Gestion Documental mas idonea del mercado en terminos de eficiencia y seguridad.
                            </p>
                            <p class="labeel" align="justify">
                              con SISVEN tu organizacion ahorrara mucho tiempo ya que integra tecnolgias de ultima generacion para gestionar documentos de la forma mas automatizada posible
                            </p>
                        </div>
                        
                    </div>
      </div>   
  </div>
</div>