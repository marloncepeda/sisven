		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Mis Archivos <?php print $title;?> </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Fecha de Creacion</th>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Etiquetas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(is_Array($archivos)){
                                                for ($i=0; $i <= $archivos[$i]["id"]; $i++) { 
                                                    echo "<tr class='odd gradeX'>";
                                                    echo "<td>".$archivos[$i]['fecha_registro']."</td>";
                                                    echo "<td><a data-id='".$archivos[$i]["id"]."' title='Abrir Acciones' class='open-AddBookDialog btn btn-success btn-block' href='#addBookDialog'>".$archivos[$i]['titulo']."</a></td>";
                                                    echo "<td>".$archivos[$i]['descripcion']."</td>";
                                                    echo "<td>".$archivos[$i]['tags']."</td>";
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="modal fade" id="addBookDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Acciones del Archivo</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/archivos_guardar_status/") ?>" method="POST">
                        <input type="text" style="visibility: hidden" name="archivo_id" id="bookId" size="5"value="" />
                        <?php if(getURL()=="http://localhost/sisven//index.php/es/sisven/archivos_papelera"){?>
                            <input type="submit" name="borrar" value="borrar" class="btn btn-danger large "/>
                        <? }else{ ?>
                            <input type="submit" name="papelera" value="Papelera" class="btn btn-danger large "/>
                        <?php } ?>
                        <input type="submit" name="publico" value="Publico" class="btn btn-success large "/>
                        <input type="submit" name="privado" value="Privado" class="btn btn-success large "/>
                        <input type="submit" name="favorito" value="Favorito" class="btn btn-warning large "/>
                        <input type="submit" name="perfil" value="Info" class="btn btn-info large "/>
                        <!--<a title='Abrir Acciones'class='open-compartir btn btn-success bt-lg' href='#compartir'>Compartir</a>-->
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="compartir" tabindex="1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Compartir Archivo</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/archivos_guardar_status/") ?>" method="POST">
                        <input type="text" style="visibility: hidden" name="archivo_id" id="bookId" size="10"value="" />
                        <input type="submit" name="papelera" value="papelera" class="btn btn-danger large "/>
                
                        
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <script type="text/javascript">
                $(document).on("click", ".open-AddBookDialog", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);
                    
                    $(_self.attr('href')).modal('show');
                });

                 $(document).on("click", ".open-compartir", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);

                    $(_self.attr('href')).modal('show');
                });
            </script>