 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php if(is_Array($info_archivo)){ ?>
                    <h1 class="page-header"><?php print $info_archivo["0"]["titulo"]; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-user fa-fw"></i> Datos Del Archivo | 
                            <a data-id='<?php print POST('archivo_id'); ?>' title='Abrir Adjuntar' class='open-AddBookDialog btn btn-success bt-lg' href='#addBookDialog'>Adjuntar</a> 
                            <a title='Descargar' class='btn btn-success' target="_blank" href="<?php print $info_archivo['0']['ruta_descargas']; ?>">Ver / Descargar</a>
                            <a data-id='<?php print POST('archivo_id'); ?>' title='Abrir Archivos Adjuntos' class='open-adjuntos btn btn-success bt-lg' href='#adjuntos'>Ver Adjuntos</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print path("sisven/archivos_modificar/") ?>" method="post">
                                <fieldset>
                                  <div class="form-group">
                                      <input type="text" style="visibility: hidden" name="archivo_id" size="10" value="<?php print POST('archivo_id'); ?>" />
                                      <input type="text" style="visibility: hidden" name="archivo_titulo_viejo"  value="<?php print $info_archivo['0']['titulo']; ?>" />
                                      <input class="form-control" name="titulo" type="text" value="<?php print $info_archivo['0']['titulo']; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="descripcion" pattern="[a-zA-Z]+" type="text" value="<?php print $info_archivo['0']['descripcion']; ?>" readonly/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="tags" pattern="[a-zA-Z]+" type="text" value="<?php print $info_archivo['0']['tags']; ?>" readonly/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" type="text" id="cant_archivo_adjunto" value="<?php print $info_archivo['0']['cant_archivo_adjunto']; ?>" readonly/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" type="email" value="<?php print $info_archivo['0']['estado']; ?>" readonly/>
                                  </div>
                                  <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>                     
                                </fieldset>
                                <?php }?>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <div class="modal fade" id="addBookDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Adjuntar Archivo</p></h4>
                  </div>
                  <div class="modal-body">
                    <form enctype="multipart/form-data" action="<?php print path("sisven/subir_archivo_adjunto/") ?>" method="post">
                      <fieldset>
                        <div class="form-group">
                          <input type="text" style="visibility: hidden" id="bookId" name="id_archivo" size="10" value="" />
                          <input type="text" style="visibility: hidden" id="cant_files" name="cant_files" size="10" value="" />
                          <input class="form-control" name="userfile" type="file" autofocus>
                        </div>
                        <input class="btn btn-lg btn-success btn-block" name="subir" type="submit" value="subir">
                      </fieldset>
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="adjuntos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Archivos Adjuntos</p></h4>
                  </div>
                  <div class="modal-body">
                      <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Creado</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(is_Array($archivos_adjuntos)){
                                                for ($i=0; $i <= $archivos_adjuntos[$i]["id"]; $i++) { 
                                                    echo "<tr class='odd gradeX'>";
                                                    echo "<td>".$archivos_adjuntos[$i]['fecha']."</td>";
                                                    echo "<td><a class='btn btn-success btn-block' href='".$archivos_adjuntos[$i]['ruta_descargas']."' target='_blank' >".$archivos_adjuntos[$i]['nombre']."</td>";
                                                    echo "</tr>";
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

              <script type="text/javascript">
                $(document).on("click", ".open-AddBookDialog", function (e) {
                  e.preventDefault();
                  var _self = $(this);
                  var myBookId = _self.data('id');
                  var cant = $('#cant_archivo_adjunto').val();
                    
                  $("#bookId").val(myBookId);
                  $("#cant_files").val(cant);

                  $(_self.attr('href')).modal('show');
                });

                $(document).on("click", ".open-adjuntos", function (e) {
                  e.preventDefault();
                  var _self = $(this);
                  var myBookId = _self.data('id');
                  var cant = $('#cant_archivo_adjunto').val();
                    
                  $("#bookId").val(myBookId);
                  $("#cant_files").val(cant);

                  $(_self.attr('href')).modal('show');
                });
            </script>