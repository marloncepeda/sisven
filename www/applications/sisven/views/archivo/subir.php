		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Subir Archivo</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <div class="container" >
              <div class="row">
                  <div class="col-md-4 col-md-offset-2">
                      <div class=" panel panel-default">
                          <div class="panel-body">
                              <form enctype="multipart/form-data" action="<?php print $href; ?>" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" name="userfile" type="file" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Nombres" name="descripcion" type="text" autofocus>
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Etiquetas" name="tags" type="text" />
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="subir" type="submit" value="subir">
                                </fieldset>
                              </form>
                          </div>
                      </div>

                  </div>
              </div>
            </div>