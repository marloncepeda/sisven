		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Listas de Usuarios</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Fecha de Creacion</th>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Etiquetas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(is_Array($usuarios)){
                                                for ($i=0; $i <= $usuarios[$i]["id"]; $i++) { 
                                                    echo "<tr class='odd gradeX'>";
                                                    echo "<td>".$usuarios[$i]['fecha_registro']."</td>";
                                                    if($usuarios[$i]["estado"]=="activo"){
                                                    echo "<td><a data-id='".$usuarios[$i]["id"]."' title='Abrir Acciones' class='open-AddBookDialog btn btn-success btn-block' href='#addBookDialog'>".$usuarios[$i]['nombre']." ".$usuarios[$i]['apellido']."</a></td>";
                                                    }else{
                                                    echo "<td><a data-id='".$usuarios[$i]["id"]."' title='Abrir Acciones' class='open-AddBookDialog btn btn-warning btn-block' href='#addBookDialog'>".$usuarios[$i]['nombre']." ".$usuarios[$i]['apellido']."</a></td>";
                                                    }
                                                    echo "<td>".$usuarios[$i]['fecha_nac']."</td>";
                                                    echo "<td>".$usuarios[$i]['correo']."</td>";
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

             <div class="modal fade" id="addBookDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Acciones del Usuario</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/usuarios_options/") ?>" method="POST">
                        <input type="text" style="visibility: hidden" name="archivo_id" id="bookId" size="10"value="" />
                        <input type="submit" name="bloquear" value="Bloquear" class="btn btn-warning large "/>
                        <input type="submit" name="borrar" value="Borrar" class="btn btn-danger large "/>
                        <input type="submit" name="desbloquear" value="Desbloquear" class="btn btn-success large "/>
                        <a class='open-registrar btn btn-success bt-lg' href='#registrar'>Crear</a>
                        
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="registrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Crear Usuario</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path("sisven/registrar/"); ?>" method="post">
                        <fieldset>
                          <div class="form-group">
                              <select class="form-control" name="nacionalidad" autofocus required>
                                <option>Seleccione una Opcion</option>
                                <option>V</option>
                                <option>P</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <input class="form-control" placeholder="Cedulade Identidad ejm: 00.000.000" name="ci" type="text" onkeyUp="return ValNumero(this);" required/>
                          </div>
                          <div class="form-group">
                              <input class="form-control" placeholder="Nombres" name="nombres" pattern="[a-zA-Z]+" type="text" required/>
                          </div>
                           <div class="form-group">
                              <input class="form-control" placeholder="Apellidos" name="apellidos" pattern="[a-zA-Z]+" type="text" required/>
                          </div>
                           <div class="form-group">
                              <input class="form-control" placeholder="Fecha de Nacimiento" name="fecha" type="date"required/>
                          </div>
                           <div class="form-group">
                              <input class="form-control" placeholder="Correo Electronico" name="email" type="email" required/>
                          </div>
                           <div class="form-group">
                              <input class="form-control" placeholder="Usuario" name="usuario" type="text"required/>
                          </div>
                          <div class="form-group">
                              <input class="form-control" placeholder="Password" name="clave" type="password" required/>
                          </div>
                          <input class="btn btn-lg btn-success btn-block" name="registro" type="submit" value="Registrar"/>
                        </fieldset>
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

             <script type="text/javascript">
                $(document).on("click", ".open-AddBookDialog", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);

                    $(_self.attr('href')).modal('show');
                });
                $(document).on("click", ".open-registrar", function (e) {

                    e.preventDefault();

                    var _self = $(this);

                    var myBookId = _self.data('id');
                    $("#bookId").val(myBookId);

                    $(_self.attr('href')).modal('show');
                });
                
                //*** Este Codigo permite Validar que sea un campo Numerico
                function Solo_Numerico(variable){
                    Numer=parseInt(variable);
                    if (isNaN(Numer)){
                        return "";
                    }
                    return Numer;
                }
                function ValNumero(Control){
                    Control.value=Solo_Numerico(Control.value);
                }
            </script>