		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Archivos Subidos Recientemente</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Fecha de Creacion</th>
                                            <th>Descripcion</th>
                                            <th>Etiquetas</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                            if(is_Array($archivos)){
                                                for ($i=0; $i < $archivos[$i]["id"]; $i++) { 
                                                    echo "<tr class='odd gradeX'>";
                                                    echo "<td>".$archivos[$i]['fecha_registro']."</td>";
                                                    echo "<td>".$archivos[$i]['titulo']."</td>";
                                                    echo "<td>".$archivos[$i]['descripcion']."</td>";
                                                    echo "<td>".$archivos[$i]['tags']."</td>";
                                                     echo "<td>".$archivos[$i]['estado']."</td>";
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
