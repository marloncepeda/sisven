 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php print SESSION("apellido").", ".SESSION("nombre")?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-user fa-fw"></i> Datos Personales
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print $href; ?>" method="post">
                                <fieldset>
                                  <div class="form-group">
                                      <input class="form-control" name="ci" type="text" value="<?php print SESSION("ci"); ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="nombres" pattern="[a-zA-Z]+" type="text" value="<?php print SESSION("nombre"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="apellidos" pattern="[a-zA-Z]+" type="text" value="<?php print SESSION("apellido"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="fecha" type="text" value="<?php print SESSION("fecha_user"); ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="email" type="email" value="<?php print SESSION("correo"); ?>" required/>
                                  </div>
                                  <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>                     
                                </fieldset>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Ultimas Acciones
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <?php 
                                    if(is_Array($logs)){
                                        for ($i=0; $i <= $logs[$i]["id"]; $i++) { 
                                            echo "<a href='#'' class='list-group-item'>";
                                            echo "<i class='fa fa-flag fa-fw'></i> ".$logs[$i]['accion'];
                                            echo "<span class='pull-right text-muted small'><em>".$logs[$i]['fecha_registro']."</em></span>";
                                            echo "</a>";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <!--
                        Donut bar pendint
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Donut Chart Example
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                            <a href="#" class="btn btn-default btn-block">View Details</a>
                        </div>
                        <!-- /.panel-body --
                    </div>-->
                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>