<?php
/**
 * Access from index.php:
 */
if(!defined("_access")) {
	die("Error: You don't have permission to access here...");
}

class sisven_Model extends ZP_Model {
	
	public function __construct() {
		$this->Db = $this->db();
		
		$this->Data = $this->core("Data");

		$this->helpers();
		$this->table_users = "users";
		$this->table_files = "archivos";
		$this->table_files_adjuntos = "archivos_adjuntos";
		$this->table_contacs = "buddy";
		$this->table_contacs_list = "buddy_list";
		$this->table_logs = "logs";
		$this->fields="id, usuario, clave";
	}
	/**************************************************************************************************
								funciones del Sistema
	***************************************************************************************************/
	public function login(){
		$_session["usuario"] = $username = POST("usuario");
		$_session["clave"] = $password = POST("clave");
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("usuario='$username' AND clave= '$password'");
	}
	
	/**************************************************************************************************
								funciones de Usuarios
	***************************************************************************************************/
	public function registrar(){
		 $data = array(
		 	"nacionalidad"=>POST("nacionalidad"),
		 	"ci"=>POST("ci"),
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"usuario"=>POST("usuario"),
		 	"clave"=>POST("clave"),
			"fecha_nac"=>POST("fecha"),
			"tipo_user"=>"users",
			"estado"=>"activo",
			"correo"=>POST("email"),
			"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("users", $data);
	}
	public function modificar_perfil($id){
		$data = array(
		 	"ci"=>POST("ci"),
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"fecha_nac"=>POST("fecha"),
			"correo"=>POST("email")
		);
		return $this->Db->update("users", $data, $id);
	}
	public function usuario_borrar($id){
		$this->Db->delete($id, $this->table_users);
	}
	public function usuario_estado($id,$status){
		$data = array(
		 	"estado"=>$status
		);
		$this->Db->update("users", $data, $id);
	}
	public function buscar_todos_usuarios(){
		return $todos_usuarios = $this->Db->findAll($this->table_users);
	}
	public function logs($accion){
		 $data = array(
		 	"ci_user"=>SESSION("ci"),
		 	"accion"=>$accion,
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("logs", $data);
	}
	public function buscar_logs($ci_user){
		$this->Db->table($this->table_logs);
		return $data = $this->Db->findBySQL("ci_user='$ci_user' ORDER BY id  DESC LIMIT 10 ");
	}	
	/**************************************************************************************************
								funciones de Archivo
	***************************************************************************************************/
	public function archivo_guardar($namefile){
		 $data = array(
		 	"titulo"=>$namefile,
		 	"descripcion"=>POST("descripcion"),
		 	"tags"=>POST("tags"),
		 	"ruta"=>"/var/www/sisven/www/lib/files/".$namefile,
		 	"ruta_descargas"=>"/sisven/www/lib/files/".$namefile,
			"user_id"=>SESSION("id"),
			"fecha_registro"=>date("d-m-Y"),
			"estado"=>"publico"
		);
		$this->Db->insert("archivos", $data);
	}
	public function archivo_adjuntar($namefile,$cant_files_ant){
		 $data = array(
		 	"id_archivo"=>POST("id_archivo"),
		 	"nombre"=>$namefile,
		 	"ruta"=>"/var/www/sisven/www/lib/files/".$namefile,
		 	"ruta_descargas"=>"/sisven/www/lib/files/".$namefile,
			"fecha"=>date("d-m-Y")
		);
		$this->Db->insert("archivos_adjuntos", $data);
		
		$actualiza = array(
		 	"cant_archivo_adjunto"=>$cant_files_ant + 1
		);
		$this->Db->update("archivos", $actualiza, $archivo_id);

	}
	public function archivos_buscar($buscar){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("titulo like '%$buscar%' OR descripcion like '%$buscar%' AND estado='privado' ");
	}
	public function archivos_todos_user($user_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("user_id='$user_id'");
	}
	public function archivos_publicos_user($user_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("user_id='$user_id' AND estado='publico' ");
	}

	public function archivos_privados_user($user_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("user_id='$user_id' AND estado='privado' ");
	}

	public function archivos_favoritos_user($user_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("user_id='$user_id' AND estado='favorito' ");
	}

	public function archivos_papelera_user($user_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("user_id='$user_id' AND estado='papelera' ");
	}

	public function archivos_ultimos_user($user_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("user_id='$user_id' ORDER BY id  DESC LIMIT 10 ");
	}
	public function archivos_guardar_status($archivo_id,$status){
		$data = array(
		 	"estado"=>$status
		);
		$this->Db->update("archivos", $data, $archivo_id);
	}
	public function archivo_buscar($archivo_id){
		$this->Db->table($this->table_files);
		return $data = $this->Db->findBySQL("id='$archivo_id'");	
	}
	public function archivo_adjuntos_buscar($archivo_id){
		$this->Db->table($this->table_files_adjuntos);
		return $data = $this->Db->findBySQL("id_archivo='$archivo_id'");	
	}
	public function archivos_cambiar_nombre($archivo_id,$archivo_nombre){
		$data = array(
		 	"titulo"=>$archivo_nombre.".pdf",
		 	"ruta"=>"/var/www/sisven/www/lib/files/".$archivo_nombre.".pdf",
		 	"ruta_descargas"=>"/sisven/www/lib/files/".$archivo_nombre.".pdf"
		);
		$this->Db->update("archivos", $data, $archivo_id);
	}
	public function archivo_borrar($archivo_id){
		$this->Db->deleteBySQL("id='$archivo_id' ","archivos");
	}	
	/**************************************************************************************************
								funciones de contacto
	***************************************************************************************************/
	public function ver_contactos($user_id){
		$this->Db->table($this->table_contacs);
		return $data = $this->Db->findBySQL("id_user='$user_id'");		
	}
	public function crear_contacto(){
		$data = array(
		 	"id_user"=>SESSION("id"),
		 	"user_id_buddy"=>POST("id_lista"),
		 	"user_id_amigo"=>POST("id_usuario"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("buddy", $data);

		$data2 = array(
		 	"id_user"=>POST("id_usuario"),
		 	"user_id_buddy"=>"",
		 	"user_id_amigo"=>SESSION("id"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("buddy", $data2);
	}
	public function borrar_contacto(){
		$id_user_borrar = POST("id_usuario");
		$id_user_solicitante = SESSION("id");

		//Borra el amigo del budddy de quien lo solicita Age >= 18 AND State = 'Active' LIMIT 5
		$this->Db->deleteBySQL("user_id_amigo='$id_user_borrar' AND id_user='$id_user_solicitante'","buddy");

		//Elimina del buddy del amigo al solicitante
		$this->Db->deleteBySQL("user_id_amigo='$id_user_solicitante' AND id_user='$id_user_borrar'","buddy");

	}	
	public function buscar_todos_buddy_list($user_id){
		$this->Db->table($this->table_contacs_list);
		return $data = $this->Db->findBySQL("id_user='$user_id'");
	}

	public function usuario_borrar_buddy_list(){
		$this->Db->delete(POST("id"), "buddy_list");
	}

	public function crear_lista(){
		$data = array(
		 	"id_user"=>SESSION("id"),
		 	"name"=>POST("nombre"),
		 	"description"=>POST("descripcion"),
		 	"fecha"=>date("d-m-Y")
		);
		$this->Db->insert("buddy_list", $data);
	}
}
