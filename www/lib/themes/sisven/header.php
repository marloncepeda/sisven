<?php 
    if(!defined("_access")) {
        die("Error: You don't have permission to access here..."); 
    }
?>
<!DOCTYPE html>
<html lang="<?php print get("webLang"); ?>">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php print $this->getTitle(); ?></title>
        
        <!--<link href="<?php //print path("vendors/css/frameworks/bootstrap/bootstrap.min.css", "zan"); ?>" rel="stylesheet">-->
        <link href="<?php print $this->themePath; ?>/css/bootstrap.css" rel="stylesheet">
        <link href="<?php print $this->themePath; ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php print $this->themePath; ?>/css/sb-admin.css" rel="stylesheet">
        <link href="<?php print $this->themePath; ?>/css/dataTables.bootstrap.css" rel="stylesheet">

        <!--<?php print $this->getCSS(); ?>-->
        
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/bootstrap-modal.js"></script>
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/sb-admin.js"></script>
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/jquery.metisMenu.js"></script>
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<?php print $this->themePath; ?>/js/dataTables.bootstrap.js"></script>
        <style type="text/css">
            body{
                background-image:url("<?php print $this->themePath; ?>/img/fondo.jpg");
            }
            .well{
                background-color: rgba(245, 245, 245, 0.4);
            }
            .logo{
                margin-top: -23px;
                height:60px;
            }
        </style>
        <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
            <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Le styles -->
    </head>

    <body>
    <?php 
    if(SESSION("tipo_user")==""){
    ?>
        <div id="wrapper">

            <nav class="navbar navbar-default-top navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php print path("sisven/index/"); ?>"><img src="<?php print $this->themePath; ?>/img/logo.png" class="logo" 
                        /></a>
                </div>
                <!-- /.navbar-header -->
            </nav>
        </div>
    <?php
    }elseif( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users") ){ ?> 
    <div id="wrapper">

        <nav class="navbar navbar-default-top navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php print path("sisven/index/"); ?>"><img src="<?php print $this->themePath; ?>/img/logo.png" class="logo" 
                        /></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!--<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <!--<li>
                            <a href="#">
                                <div>
                                    <strong>Kenit Guevara</strong>
                                    <span class="pull-right text-muted">
                                        <em>ayer</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Leer mas Notificaciones</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </ul>
                    <!-- /.dropdown-messages ->
                </li>-->
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo SESSION("nombre").", ".SESSION("apellido");?>
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php print path("sisven/perfil_usuario/"); ?>"><i class="fa fa-user fa-fw"></i> Mi Perfil</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php print path("sisven/salir/"); ?>"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        </div>
    </div>
    <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <form method="post" action="<?php print path("sisven/archivos_buscar/") ?>">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" name="buscar" class="form-control" placeholder="Buscar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </li>
                        </form>
                        <li>
                            <a href="<?php print path("sisven/subir/") ?>"><i class="fa fa-upload fa-fw"></i>Subir</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-folder fa-fw"></i>Mis Archivos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php print path("sisven/archivos_todos/") ?>">Todos</a>
                                </li>
                                <li>
                                    <a href="<?php print path("sisven/archivos_publicos/") ?>">Publicos</a>
                                </li>
                                <li>
                                    <a href="<?php print path("sisven/archivos_privados/") ?>">Privados</a>
                                </li>
                                <li>
                                    <a href="<?php print path("sisven/archivos_favoritos/") ?>">Favoritos</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php print path("sisven/archivos_papelera/") ?>"><i class="fa fa-bitbucket fa-fw"></i> Papelera</a>
                        </li>
                        <li>
                            <a href="<?php print path("sisven/ver_contactos/") ?>"><i class="fa fa-users fa-fw"></i> Contactos</a>
                        </li>
                        <?php if(SESSION("tipo_user")=="admin"){?><!-- menu admin-->
                        <li>
                            <a href="<?php print path("sisven/listas_usuarios/") ?>"><i class="fa fa-table fa-fw"></i>Lista de Usuarios</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-folder fa-fw"></i>Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php print path("sisven/reporte_usuarios_global/"); ?>">Usuarios Global</a>
                                </li>
                                <li>
                                    <a href="<?php print path("sisven/reporte_archivos_global/"); ?>">Archivos</a>
                                </li>
                                <li>
                                    <a href="<?php print path("sisven/reporte_logs_global/"); ?>">Logs</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-hdd-o fa-fw"></i>Administración<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php print path("sisven/respaldar_bd/"); ?>">Respaldar BD</a>
                                </li>
                                <li>
                                    <a href="<?php print path("sisven/respaldar_files/"); ?>">Respaldar Archivos</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php }else{} ?><!-- fin menu admin-->
                        <li>
                            <a href="<?php print path("sisven/salir/"); ?>"><i class="fa fa-power-off fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <?php }else{} ?>

